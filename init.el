(package-initialize)
(blink-cursor-mode 0)
(set-default-font "Source Code Pro 12")
(tool-bar-mode -1)
(global-linum-mode)
(load-theme 'solarized t)
(setq inhibit-startup-message t)
(setq package-enable-at-startup nil)
(setq column-number-mode t)

;; MELPA package archive
(when (>= emacs-major-version 24)
  (require 'package)
  (add-to-list 'package-archives
	       '("melpa" . "http://melpa.org/packages/")
	       t)
  (package-initialize))

;; Set emacs terminal path to $PATH on OS X
(when (memq window-system '(mac ns))
  (exec-path-from-shell-initialize))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   (quote
    ("8db4b03b9ae654d4a57804286eb3e332725c84d7cdab38463cb6b97d5762ad26" default))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
